reinstall:
	docker-compose stop && sudo rm -rf docker/db/ && docker-compose up --build -d

migrate:
	docker exec -it readium_php_1 php yii migrate --migrationPath=@yii/rbac/migrations && docker exec -it readium_php_1 php yii migrate

fixture:
	docker exec -it readium_php_1 bash command/fixture.sh

phpc:
	docker-compose exec php bash

start:
	docker-compose start

stop:
	docker-compose stop

build:
	docker-compose up --build -d

rebuild_db:
	docker exec -it readium_php_1 bash rebuild.sh
